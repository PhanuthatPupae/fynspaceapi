package datastore

import (
	"fmt"
	"os"

	datastore "fynspcace_api/datastore/interface"

	_ "github.com/bmizerany/pq"
	"github.com/jinzhu/gorm"
)

type databaseStore struct {
	Dialect, ConnectionString string
}

func NewDatabase(dialect string, connectionString string) datastore.DataStoreInterface {
	return &databaseStore{
		ConnectionString: connectionString,
		Dialect:          dialect,
	}
}

func (d *databaseStore) CreateConnection() *gorm.DB {
	dbConn, err := gorm.Open(d.Dialect, d.ConnectionString)
	if err != nil {
		fmt.Println("Failed to create database")
		fmt.Println(err)
		os.Exit(1)
	}
	return dbConn
}

func (d *databaseStore) VerifyDatabaseConection(dbConn *gorm.DB) {
	err := dbConn.DB().Ping()
	if err != nil {
		fmt.Println("Connection database Fail")
		os.Exit(1)
	}
}
