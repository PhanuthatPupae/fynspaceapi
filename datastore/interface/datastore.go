package datastore

import "github.com/jinzhu/gorm"

type DataStoreInterface interface {
	CreateConnection() *gorm.DB
	VerifyDatabaseConection(dbConn *gorm.DB)
}
