package http

import (
	"fynspcace_api/model"
	"net/http"

	"github.com/labstack/echo"
)

type artDelivery struct {
}

func NewArtDelivery(
	e *echo.Echo,

) {
	handler := &artDelivery{}

	v1 := e.Group("/v1")

	v1.GET("/art", handler.GetArtList)
	v1.GET("/user/art/:id", handler.GetArtByUserID)
}

func (handler *artDelivery) GetArtList(c echo.Context) error {
	response := [2]model.ArtResponse{}
	response[0].ArtByID = "FynSpace0001"
	response[0].ArtistName = "John"
	response[0].ID = 1
	response[0].Location = "Thailand"
	response[0].Name = "Night KK"
	response[0].Size = "100 x 200 cm"
	response[0].Technique = "Oil Painting"
	response[0].Weight = "1 Kg"
	response[0].ImagTitle = "https://images.unsplash.com/photo-1579167728798-a1cf3d595960?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=667&q=80"

	response[1].ArtByID = "FynSpace0002"
	response[1].ArtistName = "Tom"
	response[1].ID = 2
	response[1].Location = "UK"
	response[1].Name = "Europeana"
	response[1].Size = "100 x 200 cm"
	response[1].Technique = "Painting"
	response[1].Weight = "2 Kg"
	response[1].ImagTitle = "https://images.unsplash.com/photo-1579783902614-a3fb3927b6a5?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=683&q=80"

	return c.JSON(http.StatusOK, response)
}

func (handler *artDelivery) GetArtByUserID(c echo.Context) error {
	userID := c.Param("id")
	mockUserID := "1"
	response := model.LoginRes{}
	artModel := [3]model.ArtResponse{}
	artModel[0].ArtByID = "FynSpace0001"
	artModel[0].ArtistName = "John"
	artModel[0].ID = 1
	artModel[0].Location = "Thailand"
	artModel[0].Name = "Night KK"
	artModel[0].Size = "100 x 200 cm"
	artModel[0].Technique = "Oil Painting"
	artModel[0].Weight = "1 Kg"
	artModel[0].ImagTitle = "https://images.unsplash.com/photo-1460661419201-fd4cecdf8a8b?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80"

	artModel[1].ArtByID = "FynSpace0001"
	artModel[1].ArtistName = "Tooom space"
	artModel[1].ID = 2
	artModel[1].Location = "UK"
	artModel[1].Name = "Europeana"
	artModel[1].Size = "100 x 200 cm"
	artModel[1].Technique = "Painting"
	artModel[1].Weight = "2 Kg"
	artModel[1].ImagTitle = "https://images.unsplash.com/photo-1579783902614-a3fb3927b6a5?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=683&q=80"

	artModel[2].ArtByID = "FynSpace0001"
	artModel[2].ArtistName = "John Asercls"
	artModel[2].ID = 6
	artModel[2].Location = "Laos"
	artModel[2].Name = "Time"
	artModel[2].Size = "100 x 500 cm"
	artModel[2].Technique = "Painting"
	artModel[2].Weight = "99999 Kg"
	artModel[2].ImagTitle = "https://images.unsplash.com/photo-1536924940846-227afb31e2a5?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1047&q=80"

	if userID == mockUserID {
		userArtRes := model.UserArt{}
		userArtRes.Arts = artModel
		userArtRes.UserID = 1
		response.Message = "success"
		response.Result = userArtRes
		return c.JSON(http.StatusOK, response)

	}
	response.Message = "success"
	response.Result = "not found"

	return c.JSON(http.StatusOK, response)

}
