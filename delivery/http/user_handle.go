package http

import (
	"fynspcace_api/model"
	"net/http"

	"github.com/labstack/echo"
)

type userDelivery struct {
}

func NewUserDelivery(
	e *echo.Echo,

) {
	handler := &userDelivery{}

	v1 := e.Group("/v1")

	v1.POST("/login", handler.Login)
	// v1.POST("/register", handler.Register)

	v1.GET("/user/:id", handler.GetUserByID)
}

func (handler *userDelivery) Login(c echo.Context) error {
	body := new(model.LoginReq)
	c.Bind(body)
	mockEmail := "john_m@gmail.com"
	mockPassword := "12345678"
	response := model.LoginRes{}
	if body.Email == mockEmail && body.Password == mockPassword {
		response.Message = "success"
		response.Result = "Log in success"
		return c.JSON(http.StatusOK, response)
	}
	response.Message = "success"
	response.Result = "fail please check email or password"

	return c.JSON(http.StatusOK, response)
}

func (handler *userDelivery) GetUserByID(c echo.Context) error {
	userID := c.Param("id")
	mockUserID := "1"
	response := model.LoginRes{}
	if userID == mockUserID {
		userRes := model.UserDetailReponse{}
		userRes.Email = "john_m@gmail.com"
		userRes.FollowBy = "25,000"
		userRes.Following = "527"
		userRes.ID = 1

		response.Message = "success"
		response.Result = userRes
		return c.JSON(http.StatusOK, response)

	}
	response.Message = "success"
	response.Result = "not found"

	return c.JSON(http.StatusOK, response)
}
