package model

type ArtResponse struct {
	ID         int    `gorm:"primary_key" json:"id" form:"id" query:"id"`
	Location   string `json:"location" form:"location" query:"location"`
	Name       string `json:"name" form:"name" query:"name"`
	Technique  string `json:"technique" form:"technique" query:"technique"`
	Size       string `json:"size" form:"size" query:"size"`
	Weight     string `json:"weight" form:"weight" query:"weight"`
	ArtByID    string `json:"art_by_id" form:"art_by_id" query:"art_by_id"`
	ArtistName string `json:"artist_name" form:"artist_name" query:"artist_name"`
	ImagTitle  string `json:"image_title" form:"image_title" query:"image_title"`
}

type UserArt struct {
	UserID int            `json:"user_id" form:"user_id" query:"user_id"`
	Arts   [3]ArtResponse `json:"atrs" form:"atrs" query:"atrs"`
}
