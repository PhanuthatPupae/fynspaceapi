package model

type UserDetailReponse struct {
	ID        int    `gorm:"primary_key" json:"id" form:"id" query:"id"`
	Email     string `json:"email" form:"email" query:"email"`
	Following string `json:"follow" form:"follow" query:"follow"`
	FollowBy  string `json:"follow_by" form:"follow_by" query:"follow_by"`
}

type LoginReq struct {
	Email     string `json:"email" form:"email" query:"email"`
	Password string `json:"password" form:"password" query:"password"`
}

type LoginRes struct {
	Message string      `json:"status"`
	Result  interface{} `json:"result"`
}

