package util

import "time"

// EnvironmentInterface is an Interface for system environment
type EnvironmentInterface interface {
	SetDefaultTimeZone(name string)
	Now() (time.Time, error)
}
