package util

import (
	"time"

	util "fynspcace_api/util/interface"
)

type environment struct {
	t time.Time
}

func NewEnvironment() util.EnvironmentInterface {
	return &environment{}
}

func (m *environment) SetDefaultTimeZone(name string) {
	loc, _ := time.LoadLocation(name)
	time.Local = loc
}

// Get Bangkok time
func (m *environment) Now() (time.Time, error) {
	now := time.Now()
	return now, nil
}
