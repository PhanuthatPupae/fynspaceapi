package main

import (
	"fmt"
	"fynspcace_api/datastore"
	"fynspcace_api/delivery/http"
	"fynspcace_api/util"
	"os"

	// "strings"

	"github.com/labstack/echo"
	"github.com/spf13/viper"
)

func init() {

	viper.SetConfigFile("config.json")
	err := viper.ReadInConfig()

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

}

func main() {

	environment := util.NewEnvironment()
	environment.SetDefaultTimeZone(viper.GetString("server.timezone"))

	//select env
	// serverEnvironment := viper.GetString("environment")

	dbConfigMap := viper.GetViper().GetStringMapString("database")

	// log := util.NewLogger("logs/api.log",
	// 	dbConfigMap["loggly"],
	// 	"api",
	// 	serverEnvironment,
	// )

	// log.Info("started the api, parameter(s)=[", strings.Join(os.Args[1:], ", "), "]")
	connectionMysqlString := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s TimeZone=%s",
		dbConfigMap["host"],
		dbConfigMap["port"],
		dbConfigMap["user"],
		dbConfigMap["pass"],
		dbConfigMap["name"],
		dbConfigMap["sslmode"],
		dbConfigMap["timeZone"],
	)

	dbDatastore := datastore.NewDatabase(dbConfigMap["dialect"], connectionMysqlString)
	dbConn := dbDatastore.CreateConnection()
	dbDatastore.VerifyDatabaseConection(dbConn)
	// defer dbConn.Close()
	// log.Info(fmt.Sprintf("connected to the database at %s", dbConfigMap["host"]))

	// logDBConfigMap := viper.GetViper().GetStringMapString("log_database")
	// connectionMysqlStringLog := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
	// 	logDBConfigMap["host"],
	// 	logDBConfigMap["port"],
	// 	logDBConfigMap["user"],
	// 	logDBConfigMap["pass"],
	// 	logDBConfigMap["name"],
	// 	logDBConfigMap["sslmode"],
	// )

	// logDBDatastore := datastore.NewDatabase(logDBConfigMap["dialect"], connectionMysqlStringLog)
	// logDBConn := dbDatastore.CreateConnection()
	// logDBConn.DB().SetMaxOpenConns(17)
	// logDBDatastore.VerifyDatabaseConection(logDBConn)
	// defer logDBConn.Close()
	// log.Info(fmt.Sprintf("connected to the log database at %s", logDBConfigMap["host"]))

	e := echo.New()

	e.GET("/", func(context echo.Context) error {
		envTime, _ := environment.Now()
		jsonRespone := fmt.Sprintf(" Test API[%s] - %s ",
			viper.GetString("environment"),
			envTime.String(),
		)
		return context.JSON(200, jsonRespone)
	})

	http.NewArtDelivery(e)
	http.NewUserDelivery(e)
	// e.POST("v1/auth/token/refresh", echo.HandlerFunc(func(c echo.Context) error {
	// 	var err error
	// 	util.NewToken().RefreshToken(c)
	// 	// return c.String(http.StatusOK, "Hello, World!")
	// 	return err
	// }))

	e.Logger.Fatal(e.Start(viper.GetString("server.address")))
}
