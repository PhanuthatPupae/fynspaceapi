package login

const (
	ACCOUNT_INACTIVE_NUMBER = iota
	ACCOUNT_ACTIVE_NUMBER
	ACCOUNT_NOT_ACTIVE_NUMBER
	SUCCESS                = "success"
	FAIL                   = "Fail"
	INVALID_EMAIL          = "InValid Email"
	ACCOUNT_INACTIVE       = "Account InActive"
	INVALID_PASSWORD       = "Invalid Password"
	ACCOUNT_NOT_ACTIVE     = "Account NotActive"
	CREAT_TOKEN_FAIL       = "Create Token Fail"
	UPDATE_TOKEN_FAIL      = "Update Token Fail"
	INTERNAL_SERVER_ERROR  = "Internal Server Error"
	FAIL_INSERT_LOG        = "Fail insert logs"
	SETUP_PASSWORD_SUCCESS = "Setup Password Success"
)
