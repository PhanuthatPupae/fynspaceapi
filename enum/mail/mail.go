package mail

const (
	MAIL_NOT_ACTIVATE_NUMBER = iota
	MAIL_ACTIVATE_NUMBER
	MAIL_INACTIVATE    = "InActivate"
	MAIL_ACTIVATE      = "Activate"
	MAIL_NOT_ACTIVATE  = "NotActivate"
	COMPLETE_SEND_MAIL = "Complete Send mail"
	NOT_FOUND_MAIL     = "Email Not Found"
)
