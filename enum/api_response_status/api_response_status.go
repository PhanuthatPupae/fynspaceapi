package APIResponseStatus

// APIResponseStatus enum -------------------------
type APIResponseStatus int32

type ResponseStatus struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

// for APIResponseStatus
const (
	STATUS_ERROR_INVALID_EMAIL APIResponseStatus = iota
	STATUS_ACCOUNT_INACTIVATE
	STATUS_ERROR_INVALID_PASSWORD
	STATUS_ACCOUNT_NOT_ACTIVATE
	STATUS_ERROR_CREATE_TOKEN_FAIL
	STATUS_ERROR_UPDATE_TOKEN_FAIL
	STATUS_ERROR_FAIL_INSERT_LOG
	STATUS_ERROR_SERVER
	STATUS_ERROR_UNAUTHORIZED
	STATUS_ERROR_DATA_NOT_FOUND
	STATUS_ACTIVATE
	STATUS_MAIL_INACTIVATE
	STATUS_MAIL_ACTIVATE
	STATUS_MAIL_NOTACTIVATE
	STATUS_SETUP_PASSWORD
	STATUS_ADD_TO_MY_SUPPLIER
	STATUS_DELETE_TO_MY_SUPPLIER
	STATUS_ADD_RFQ
	STATUS_DELETE_RFQ
	STATUS_SENDMAIL_SUCCESS
	STATUS_DELETE_COMPLETE
	STATUS_UPDATE_COMPLETE
	STATUS_EMAIL_DUPLICATE
	STATUS_ERROR_VERTIFY_NOT_FOUND_DATA
	STATUS_INSERT_SUCCESS
	STATUS_UPDATE_SUCCESS
	STATUS_DELETE_SUCCESS
	STATUS_INSERT_FAILED
	STATUS_DO_NOT_HAVE_ID
	STATUS_UPLOAD_SUCCESS
	STATUS_INVALID_MAIL_KEY
	STATUS_TAX_ID_DUPLICATE
	STATUS_VALID
	STATUS_DATA_DUPLICATE
	STATUS_ACCOUNT_INVALID
	STATUS_TOKEN_EXPIRED
	STATUS_TAX_ID_NOT_FOUND
	STATUS_DONT_HAVE_CONTENT
	STATUS_INVALID_STATUS
	STATUS_NOT_FOUND_MAIL_KEY
	STATUS_FILE_NAME_EXIST
	STATUS_INVALID_FILE_EXT
	STATUS_PARSE_FILE_ERROR
	STATUS_ALREADY_ACCOUNT
)

// APIResponseStatusList for map between key and its corresponding result structure
var APIResponseStatusList = map[APIResponseStatus]ResponseStatus{
	// STATUS_ERROR_INVALID_EMAIL means email wrong
	STATUS_ERROR_INVALID_EMAIL: ResponseStatus{
		Status:  "InvalidEmail",
		Message: "Email is not Registered",
	},
	// STATUS_ACCOUNT_INACTIVE
	STATUS_ACCOUNT_INACTIVATE: ResponseStatus{
		Status:  "InActivate",
		Message: "Sorry , Account InActive By Admin",
	},
	// STATUS_ACCOUNT_INACTIVE
	STATUS_ERROR_INVALID_PASSWORD: ResponseStatus{
		Status:  "InvalidPassword",
		Message: "Invalid Password",
	},
	// STATUS_ERROR_ACCOUNT_NOT_ACTIVE
	STATUS_ACCOUNT_NOT_ACTIVATE: ResponseStatus{
		Status:  "NotActivate",
		Message: "Account Not Active Please Verify Email",
	},
	STATUS_ERROR_CREATE_TOKEN_FAIL: ResponseStatus{
		Status:  "400",
		Message: "Create Token Fail",
	},
	STATUS_ERROR_UPDATE_TOKEN_FAIL: ResponseStatus{
		Status:  "400",
		Message: "Update Token Fail",
	},
	STATUS_ERROR_FAIL_INSERT_LOG: ResponseStatus{
		Status:  "500",
		Message: "Insert Log Fail",
	},
	STATUS_ERROR_UNAUTHORIZED: ResponseStatus{
		Status:  "401",
		Message: "unauthorized",
	},
	STATUS_ERROR_SERVER: ResponseStatus{
		Status:  "500",
		Message: "Internal Server Error",
	},
	STATUS_ACTIVATE: ResponseStatus{
		Status:  "Activate",
		Message: "Email Activate",
	},
	STATUS_ERROR_DATA_NOT_FOUND: ResponseStatus{
		Status:  "NotFouund",
		Message: "Data Not Found",
	},
	STATUS_ERROR_VERTIFY_NOT_FOUND_DATA: ResponseStatus{
		Status:  "NotFouund",
		Message: "TaxID Not Found",
	},
	STATUS_MAIL_INACTIVATE: ResponseStatus{
		Status:  "InActivate",
		Message: "Email InActivate",
	},
	STATUS_MAIL_ACTIVATE: ResponseStatus{
		Status:  "Activate",
		Message: "Email Activate",
	},
	STATUS_MAIL_NOTACTIVATE: ResponseStatus{
		Status:  "NotActivate",
		Message: "Email Not Activate",
	},
	STATUS_SETUP_PASSWORD: ResponseStatus{
		Status:  "Success",
		Message: "SetUp Password Complete",
	},
	STATUS_ADD_TO_MY_SUPPLIER: ResponseStatus{
		Status:  "AddSuccess",
		Message: "Add To My Supplier Complete",
	},
	STATUS_DELETE_TO_MY_SUPPLIER: ResponseStatus{
		Status:  "DeleteSuccess",
		Message: "Delete My Supplier Complete",
	},
	STATUS_ADD_RFQ: ResponseStatus{
		Status:  "AddSuccess",
		Message: "Add To Rfq Complete",
	},
	STATUS_DELETE_RFQ: ResponseStatus{
		Status:  "DeleteSuccess",
		Message: "Delete Rfq Complete",
	},
	STATUS_UPLOAD_SUCCESS: ResponseStatus{
		Status:  "UploadSuccess",
		Message: "Upload File Complete",
	},
	STATUS_SENDMAIL_SUCCESS: ResponseStatus{
		Status:  "SendMailSuccess",
		Message: "Send Mail Complete",
	},
	STATUS_DELETE_COMPLETE: ResponseStatus{
		Status:  "DeleteSuccess",
		Message: "Delete User Complete",
	},
	STATUS_UPDATE_COMPLETE: ResponseStatus{
		Status:  "UpdateSuccess",
		Message: "Update User Complete",
	},
	STATUS_EMAIL_DUPLICATE: ResponseStatus{
		Status:  "EmailDuplicate",
		Message: "Email duplicate please change email",
	},
	STATUS_INSERT_SUCCESS: ResponseStatus{
		Status:  "InSertSuccess",
		Message: "Insert Data Complete",
	},
	STATUS_UPDATE_SUCCESS: ResponseStatus{
		Status:  "UpdateSuccess",
		Message: "Update Data Complete",
	},
	STATUS_DELETE_SUCCESS: ResponseStatus{
		Status:  "DeleteSuccess",
		Message: "Delete Data Complete",
	},
	STATUS_INSERT_FAILED: ResponseStatus{
		Status:  "InSertFailed",
		Message: "Insert Data Failed",
	},
	STATUS_DO_NOT_HAVE_ID: ResponseStatus{
		Status:  "DontHaveID",
		Message: "Don't Have Id",
	},
	STATUS_INVALID_MAIL_KEY: ResponseStatus{
		Status:  "InvalidMailKey",
		Message: "invalid mailkey",
	},
	STATUS_TAX_ID_DUPLICATE: ResponseStatus{
		Status:  "TaxIDDuplicate",
		Message: "TaxID Duplicate",
	},
	STATUS_TAX_ID_NOT_FOUND: ResponseStatus{
		Status:  "TaxIDNotFound",
		Message: "TaxID Not Found",
	},
	STATUS_VALID: ResponseStatus{
		Status:  "TokenValid",
		Message: "Token valid",
	},
	STATUS_DATA_DUPLICATE: ResponseStatus{
		Status:  "Duplicate",
		Message: "Data Duplicate",
	},
	STATUS_ACCOUNT_INVALID: ResponseStatus{
		Status:  "Invalid",
		Message: "Account Invalid",
	},
	STATUS_TOKEN_EXPIRED: ResponseStatus{
		Status:  "TokenExpired",
		Message: "Token Expired",
	},
	STATUS_DONT_HAVE_CONTENT: ResponseStatus{
		Status:  "SpreadSheet",
		Message: "Dont Have Content",
	},
	STATUS_INVALID_STATUS: ResponseStatus{
		Status:  "INVALID",
		Message: "Status Supplier No Not regis OR Waiting Activate",
	},
	STATUS_NOT_FOUND_MAIL_KEY: ResponseStatus{
		Status:  "NOTFOUND",
		Message: "Mail Key Not Found",
	},
	STATUS_FILE_NAME_EXIST: ResponseStatus{
		Status:  "FileNameExist",
		Message: "The file name already exist",
	},
	STATUS_INVALID_FILE_EXT: ResponseStatus{
		Status:  "InvalidFileExtension",
		Message: "File extension is not allowed",
	},
	STATUS_PARSE_FILE_ERROR: ResponseStatus{
		Status:  "ParseFileError",
		Message: "File cannot be parsed",
	},
	STATUS_ALREADY_ACCOUNT: ResponseStatus{
		Status:  "Already Account",
		Message: "You already have an account",
	},
}
