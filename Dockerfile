FROM golang:1.15.0-alpine3.12
RUN apk update && apk add gcc git
COPY . /go/src/fynspcace_api
WORKDIR /go/src/fynspcace_api/app/api
RUN go get
RUN go build api.go
CMD ["./api","-e","dev"]