module fynspcace_api

go 1.15

require (
	github.com/360EntSecGroup-Skylar/excelize v1.4.1
	github.com/bmizerany/pq v0.0.0-20131128184720-da2b95e392c1
	github.com/bradfitz/slice v0.0.0-20180809154707-2b758aa73013
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/dustin/go-humanize v1.0.0
	github.com/google/uuid v1.1.2
	github.com/jinzhu/gorm v1.9.16
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible
	github.com/lestrrat-go/strftime v1.0.3 // indirect
	github.com/lib/pq v1.9.0
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e
	github.com/nsqio/go-nsq v1.0.8
	github.com/olivere/elastic/v7 v7.0.22
	github.com/rifflock/lfshook v0.0.0-20180920164130-b9218ef580f5
	github.com/robfig/cron/v3 v3.0.1
	github.com/segmentio/go-loggly v0.5.0
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/viper v1.7.1
	github.com/tkanos/gonfig v0.0.0-20210106201359-53e13348de2f // indirect
	github.com/twinj/uuid v1.0.0
	github.com/valyala/fasttemplate v1.2.1 // indirect
	golang.org/x/crypto v0.0.0-20201217014255-9d1352758620
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.21.3

)
